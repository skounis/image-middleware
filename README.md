# README #

An S3 facade which gets a request for an image stored in S3. The application receives the request for a specific image and proceed with its cropping and resizing according to given instructions. 

The derived image is stored again in S3 and a reference to the derived image is returned. 

All these are happening complete transparent for the client that send the request who received back just the response for the requested image URL.