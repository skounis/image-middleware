'use strict';

var dotenv = require('dotenv');
dotenv.load();

var util = require('util');
var stream = require('stream');
var express = require('express');
var app = express();
var ir = require('image-resizer');
var env = ir.env;
var Img = ir.img;
var streams = ir.streams;
var concat = require('concat-stream');
var knox = require('knox');

var s3 = knox.createClient({
  key: process.env['AWS_ACCESS_KEY_ID'],
  secret: process.env['AWS_SECRET_ACCESS_KEY'],
  bucket: process.env['S3_BUCKET']
});

app.directory = __dirname;
ir.expressConfig(app);

/**
Return the modifiers map as a documentation endpoint
*/
app.get('/modifiers.json', function(request, response) {
  response.status(200).json(ir.modifiers);
});

/**
Some helper endpoints when in development
*/
if (env.development) {
  // Show the environment variables and their current values
  app.get('/env', function(request, response){
    response.status(200).json(env);
  });
}

/*
Return an image modified to the requested parameters
  - request format:
    /:modifers/path/to/image.format:metadata
    eg: https://doapv6pcsx1wa.cloudfront.net/s50/sample/test.png
*/
app.get('/*?', function(request, response) {
  if (request.path.indexOf('/original/') == 0) {
    response.redirect(s3.http(request.path.substr(10)));
  } else if (typeof request.query.flush !== 'undefined') {
    cache_and_redirect(request, response);
  } else {
    // check cache
    s3.head('_cache' + request.path).on('response', function(res) {
      if (res.statusCode == 200) {
        // if cached version exists, just redirect
        response.redirect(s3.http('_cache' + request.path));
      } else {
        // save cached version and redirect
        cache_and_redirect(request, response);
      }
    }).end();
  }
});

function cache_and_redirect(request, response) {
  var image = new Img(request);

  image.getFile()
    .pipe(new streams.identify())
    .pipe(new streams.resize())
    //.pipe(new streams.filter()) // uncomment to get filters
    //.pipe(new streams.optimize()) // uncomment to get optimization
    //.pipe(streams.response(request, response));
    .pipe(new Uploader(request, response));
};

function Uploader(request, response){
  if (!(this instanceof Uploader)){
    return new Uploader(request, response);
  }

  this.request = request;
  this.response = response;

  stream.Writable.call(this, { objectMode : true });
}

util.inherits(Uploader, stream.Writable);

Uploader.prototype._write = function(image){
  if (image.isError()){
    image.log.error(image.error.message);
    image.log.flush();
    var statusCode = image.error.statusCode || 500;
    this.response.status(statusCode).end();
    return;
  }

  if (image.modifiers.action === 'json'){
    this.response.status(200).json(image.contents);
    image.log.flush();

    return this.end();
  }

  var _this = this;
  image.contents.pipe(concat(function(buffer) {
    s3.putBuffer(buffer, '_cache' + _this.request.path, {}, function(err, res) {
      if (err) {
        _this.response.end();
      } else {
        _this.response.redirect(s3.http('_cache' + _this.request.path));
      }
    });
  }));

  this.end();
};

/**
Start the app on the listed port
*/
app.listen(app.get('port'));
